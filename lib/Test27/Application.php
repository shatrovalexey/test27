<?php
/**
* Фасад для задачи
* @see https://bitbucket.org/shatrovalexey/test27
* @author Shatrov Aleksej <mail@ashatrov.ru>
* @package Test27 описанные классы задачи
*/

namespace Test27;

class Application {
	/**
	* @const DOM_VERSION string версия DOM
	* @const CHARSET string кодировка файлов
	* @const DEBUG_FILE_NAME string путь к ноде для вывода STDERR по-умолчанию
	* @var success_query string запрос XPath для проверки отсутствия сообщений об ошибках
	* @var success_string string сообщение об ошибке
	* @var sequence_length string максимальная длина последовательности для подбора
	* @var url string URL для запросов
	* @var max_connections string максимальное количество подключений
	* @var mh resource ссылка на множественный CURL
	* @var retryes array список ключей для повторных запросов
	* @var connection_count int текущее количество активных CURL-запросов
	* @var debug_fh resource дескриптор для STDERR
	* @var debug_file_name string путь к STDERR
	*/
	const DOM_VERSION = '1.0' ;
	const CHARSET = 'utf-8' ;
	const DEBUG_FILE_NAME = 'php://stderr' ;
	private $success_query = '//body[not(contains(text( ),"%s"))]/text()' ;
	private $success_string ;
	private $sequence_length ;
	private $url ;
	private $max_connections ;
	private $mh ;
	private $retryes = array( ) ;
	private $connection_count = 0 ;
	private $debug_fh ;
	private $debug_file_name ;

	function __construct( $url , $success_string , $sequence_length = 1 , $max_connections = 1e2 , $debug_file_name ) {
		/**
		* Конструктор класса
		* @param url string URL для запросов
		* @param success_string string сообщение об ошибке
		* @param sequence_length string максимальная длина последовательности для подбора
		* @param max_connections string максимальное количество подключений
		* @param debug_file_name string путь к STDERR
		* @return void
		*/
		$this->success_string = $success_string ;
		$this->sequence_length = $sequence_length ;
		$this->url = $url ;
		$this->max_connections = $max_connections ;
		$this->debug_file_name = $debug_file_name ;
	}

	private function __destruct( ) {
		/**
		* Деструктор класса
		* @return void
		*/
		fclose( $this->debug_fh ) ;
	}

	private function put_log( $data ) {
		/**
		* Создание записи в журнале наблюдений
		* @param data mixed данные для записи
		* @return int
		*/
		return fputs( $this->debug_fh , json_encode( $data ) ) ;
	}

	private function get_sequence( $result = '' ) {
		/**
		* Генератор последовательностей
		* @param result string текущая последовательность
		*/
		foreach ( range( 0 , 9 ) as $char ) {
			foreach ( $this->get_retryes( ) as $sequence ) {
				yield $sequence ;
			}

			$sequence = $result . $char ;

			if ( strlen( $sequence ) >= $this->sequence_length ) {
				yield $sequence ;

				continue ;
			}

			foreach ( $this->get_sequence( $sequence ) as $value ) {
				yield $value ;
			}
		}

		if ( ! empty( $result ) ) {
			return ;
		}

		foreach ( $this->get_retryes( ) as $sequence ) {
			yield $sequence ;
		}
	}

	private function get_retryes( ) {
		/**
		* Генератор для получения последовательностей, которые нужно обработать повторно
		*/
		while ( $sequence = array_shift( $this->retryes ) ) {
			yield $sequence ;
		}
	}

	private function full_curl_multi_exec( $mh , &$is_active ) {
		/**
		* Ожидание обработки набора запросов
		* @param mh resource множественный CURL
		* @param is_active bool признак активности обработки
		* @return int
		*/
		do {
			$mrc = curl_multi_exec( $mh , $is_active ) ;
		} while ( $mrc == CURLM_CALL_MULTI_PERFORM ) ;

		return $mrc ;
	}

	private function perform_curl_multi( ) {
		/**
		* Обработка набора запросов
		* @return void
		*/
		$mrc = $this->full_curl_multi_exec( $this->mh , $is_active ) ;

		while ( $is_active && ( $mrc == CURLM_OK ) ) {
			curl_multi_select( $this->mh ) ;
			$mrc = $this->full_curl_multi_exec( $this->mh , $is_active ) ;

			while ( $info = curl_multi_info_read( $this->mh ) ) {
				$ch = $info[ 'handle' ] ;
				foreach ( $this->check_if_found( $ch ) as $code ) {
					$this->retryes[] = $code ;
				}
				curl_multi_remove_handle( $this->mh , $ch ) ;

				$this->connection_count -- ;
			} 
		}
	}

	private function get_curl_handle( $code ) {
		/**
		* Создание "обещания" CURL на обработку запроса
		* @param code string последовательность для проверки
		* @return resource - "обещание"
		*/
		$query = http_build_query( array( 'code' => $code ) ) ;
		$ch = curl_init( implode( '?' , array( $this->url , $query ) ) ) ;
		curl_setopt_array( $ch , array(
			CURLOPT_RETURNTRANSFER => true ,
			CURLOPT_POST => true ,
			CURLOPT_POSTFIELDS => $query
		) ) ;

		return $ch ;
	}

	private function check_if_found( $ch ) {
		/**
		* Генератор. Проверка, если запрос вернул искомый результат
		* @param ch resource "обещание" CURL проверить запрос
		* @throws \Exception
		*/
		parse_str( parse_url( curl_getinfo( $ch , CURLINFO_EFFECTIVE_URL ) , PHP_URL_QUERY ) , $query ) ;

		if ( ( $http_code = curl_getinfo( $ch , CURLINFO_HTTP_CODE ) ) != 200 ) {
			$this->put_log( array(
				$query[ 'code' ] => $http_code
			) ) ;

			yield $query[ 'code' ] ;

			return ;
		}

		foreach ( $this->document( $ch )->query( $this->success_query ) as $text ) {
			throw new \Exception( json_encode( array(
				$query[ 'code' ] => $text->wholeText
			) ) ) ;
		}

		$this->put_log( array(
			$query[ 'code' ] => 'wrong'
		) ) ;
	}

	private function document( $ch ) {
		/**
		* Создание объекта DOMXPath из ответа на запрос CURL
		* @param ch resource "обещание" CURL проверить запрос
		* @return DOMXPath
		*/
		$domh = new \DOMDocument( self::DOM_VERSION , self::CHARSET ) ;
		@$domh->loadHTML( curl_multi_getcontent( $ch ) ) ;

		return new \DOMXPath( $domh ) ;
	}

	public function perform( ) {
		/**
		* Реализация задачи класса
		* @return \Test27\Application
		*/
		return $this->prepare( )->execute( )->finish( ) ;
	}

	private function prepare( ) {
		/**
		* Подготовка к реализации задачи класса
		* @return \Test27\Application
		* @throws \Exception
		*/
		$success_string = htmlspecialchars( $this->success_string , ENT_XML1 , self::CHARSET ) ;
		$this->success_query = sprintf( $this->success_query , $success_string ) ;

		if ( empty( $this->debug_file_name ) ) {
			$this->debug_file_name = self::DEBUG_FILE_NAME ;
		}

		if ( ! ( $this->debug_fh = fopen( $this->debug_file_name , 'wb' ) ) ) {
			throw new \Exception( 'Couldn\'t open "' . $this->debug_file_name . '"' ) ;
		}

		return $this ;
	}

	private function execute( ) {
		/**
		* Выполнение задачи класса
		* @return \Test27\Application
		*/
		$this->mh = curl_multi_init( ) ;
		curl_multi_setopt( $this->mh , CURLMOPT_MAXCONNECTS , $this->max_connections ) ;
		curl_multi_setopt( $this->mh , CURLMOPT_PIPELINING , 2 ) ;

		foreach( $this->sequence( ) as $code ) {
			$this->add_curl_multi( $code ) ;
		}

		return $this ;
	}

	private function finish( ) {
		/**
		* Завершение выполнения задачи класса
		* @return \Test27\Application
		*/
		while ( $this->retryes || $this->connection_count ) {
			$this->perform_curl_multi( ) ;

			foreach ( $this->get_retryes( ) as $code ) {
				$this->add_curl_multi( $code ) ;
			}
		}

		return $this ;
	}

	private function add_curl_multi( $code ) {
		/**
		* Завершение выполнения задачи класса
		* @param code string код для подбора
		* @return int количество "обещаний" CURL
		*/
		if ( ! ( $this->connection_count %= $this->max_connections ) ) {
			$this->perform_curl_multi( ) ;
		}

		curl_multi_add_handle( $this->mh , $this->get_curl_handle( $code ) ) ;

		return ++ $this->connection_count ;
	}
}