### ЗАДАЧА ###
http://snapsa9q.beget.tech/test.php
Нужно написать программу на php( без фреймов с применением ООП), которая будет подбирать код (только цифры) методом перебора.
Если код введён правильно, ответом будет линк на википедию.
2 требования к ответу:
1) Правильный линк на вики
2) Программа должна подбирать пароль за максимум пол часа

### ЗАПУСК ###
`composer install` &&
`php 'bin/run.php'`

### ПРОБЛЕМА ###
Сервер не держит больше 10 подключений от одного IP. Нужно добавить поддержку многих прокси.